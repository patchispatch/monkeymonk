extends PlayerState

class_name MoveState

# TODO: split into separate functions, don't override physics_process unless needed
func physics_process(delta) -> BaseState:
    # Physics logic
    var move = get_movement_input()

    player.velocity.y += player.gravity * delta
    player.velocity.x = move * player.move_speed

    # Apply gravity and move
    super(delta)

    # Check transitions
    if move == 0:
        return transition_on_idle()

    return null


func get_movement_input() -> int:
    var move = 0

    if Input.is_action_pressed("move_left"):
        move -= 1
    
    if Input.is_action_pressed("move_right"):
        move += 1
    
    return move


func transition_on_idle() -> PlayerState:
    return idle_state


# func transition_on_kick() -> PlayerState:
#     return kick_state