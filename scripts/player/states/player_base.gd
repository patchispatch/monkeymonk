extends BaseState

class_name PlayerState

# Reference to other state nodes
@export var idle_state: PlayerState
@export var jump_state: PlayerState
@export var run_state: PlayerState
@export var kick_state: PlayerState


var player: Player

func init(bearer: Node):
    player = bearer


# Handle transitions on action input
func handle_input(event: InputEvent) -> BaseState:
    if event.is_action_pressed("move_left") or event.is_action_pressed("move_right"):
        return transition_on_move()
    elif event.is_action_pressed("jump"):
        return transition_on_jump()
    elif event.is_action_pressed("kick"):
        return transition_on_kick()
    return transition_on_idle()



func physics_process(delta) -> BaseState:
    # Apply physics
    apply_gravity(delta)

    # Move
    player.move_and_slide()

    # Transition
    return null


# Physics

func apply_gravity(delta) -> void:
    # TODO: PROPERLY DO THIS
    if player.is_on_floor():
        player.velocity.y = 0
    else:
        player.velocity.y += player.gravity * delta


# Actions
# Init with null and explicitly specify on each state 

func transition_on_jump() -> PlayerState:
    return null


func transition_on_move() -> PlayerState:
    return null


func transition_on_kick() -> PlayerState:
    return null


func transition_on_idle() -> PlayerState:
    return null