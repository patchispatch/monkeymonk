extends PlayerState


func enter() -> void:
	super()
	player.velocity.x = 0


func transition_on_jump() -> PlayerState:
	return jump_state


func transition_on_move() -> PlayerState:
	return run_state


# func transition_on_kick() -> PlayerState:
# 	return kick_state