extends MoveState


func handle_input(event: InputEvent) -> BaseState:
    if event.is_action_pressed("jump"):
        return transition_on_jump()
    return null


func transition_on_jump() -> PlayerState:
    return jump_state

