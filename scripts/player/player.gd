extends CharacterBody2D

class_name Player


@export var move_speed = 100.0
@export var jump_impulse = -200.0

# @onready var animations = $Animations
@onready var states = $StateManager

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")


func _ready() -> void:
	# Initialize the state machine, passing a reference of the player to the states,
	# that way they can move and react accordingly
	states.init(self)

	# Initialize player
	velocity = Vector2.ZERO


func _unhandled_input(event: InputEvent) -> void:
	states.handle_input(event)


func _physics_process(delta: float) -> void:
	states.physics_process(delta)


func _process(delta: float) -> void:
	states.process(delta)
