extends Node

@export var starting_state: BaseState

var current_state: BaseState


func change_state(new_state: BaseState) -> void:
	if current_state:
		current_state.exit()

	current_state = new_state
	current_state.enter()


# Initialize the state machine by giving each state a reference to the Bearer
# so it can expose the required data
func init(bearer: Node) -> void:
	for child in get_children():
		child.init(bearer)

	# Initialize with a default state of idle
	change_state(starting_state)


# Pass through functions for the Bearer to call,
# handling state changes as needed
func physics_process(delta: float) -> void:
	var new_state = current_state.physics_process(delta)
	if new_state:
		change_state(new_state)


func handle_input(event: InputEvent) -> void:
	var new_state = current_state.handle_input(event)
	if new_state:
		change_state(new_state)


func process(delta: float) -> void:
	var new_state = current_state.process(delta)
	if new_state:
		change_state(new_state)
