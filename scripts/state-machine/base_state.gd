extends Node

class_name BaseState

# Abstract base state class for state machine.


# Init state
func init(_bearer: Node):
    push_error("Not implemented in subclass")


func enter() -> void:
    pass


func exit() -> void:
    pass


func handle_input(_event: InputEvent) -> BaseState:
    return null


func process(_delta: float) -> BaseState:
    return null


func physics_process(_delta: float) -> BaseState:
    return null